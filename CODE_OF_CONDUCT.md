# Contributor Covenant Code of Conduct

## Our Pledge

We as members, contributors, and leaders pledge to make participation in our
community a harassment-free experience for everyone, regardless of age, body
size, visible or invisible disability, ethnicity, sex characteristics, gender
identity and expression, level of experience, education, socio-economic status,
nationality, personal appearance, race, religion, or sexual identity
and orientation.

We pledge to act and interact in ways that contribute to an open, welcoming,
diverse, inclusive, and healthy community.

## Our Standards

Examples of behavior that contributes to a **positive** environment for our
community include:

* Demonstrating empathy and kindness toward other people

* Being respectful of differing opinions, viewpoints, and experiences

* Giving and gracefully accepting constructive feedback

* Accepting responsibility and apologizing to those affected by our mistakes,
  and learning from the experience

* Focusing on what is best not just for us as individuals, but for the
  overall community

Examples of **unacceptable behavior** include:

* The use of sexualized language or imagery, and sexual attention or
  advances of any kind

* Trolling, insulting or derogatory comments, and personal or political attacks

* Public or private harassment

* Publishing others' private information, such as a physical or email
  address, without their explicit permission

* Other conduct which could reasonably be considered inappropriate in a
  professional setting

## Enforcement Responsibilities

Community leaders are responsible for clarifying and enforcing our standards of
acceptable behavior and will take appropriate and fair corrective action in
response to any behavior that they deem inappropriate, threatening, offensive,
or harmful.

Community leaders have the right and responsibility to remove, edit, or reject
comments, commits, code, wiki edits, issues, and other contributions that are
not aligned to this Code of Conduct, and will communicate reasons for moderation
decisions when appropriate.

## Scope

This Code of Conduct applies within all community spaces, and also applies when
an individual is officially representing the community in public spaces.
Examples of representing our community include using an official e-mail address,
posting via an official social media account, or acting as an appointed
representative at an online or offline event.

## Enforcement

Instances of abusive, harassing, or otherwise unacceptable behavior may be
reported to the community leaders responsible for enforcement at
[code-of-conduct@swindle.net][code-of-conduct-contact].
All complaints will be reviewed and investigated promptly and fairly.

All community leaders are obligated to respect the privacy and security of the
reporter of any incident.

## Enforcement Guidelines

Community leaders will follow these Community Impact Guidelines in determining
the consequences for any action they deem in violation of this Code of Conduct:

### 1. Correction

**Community Impact**: Use of inappropriate language or other behavior deemed
unprofessional or unwelcome in the community.

**Consequence**: A private, written warning from community leaders, providing
clarity around the nature of the violation and an explanation of why the
behavior was inappropriate. A public apology may be requested.

### 2. Warning

**Community Impact**: A violation through a single incident or series
of actions.

**Consequence**: A warning with consequences for continued behavior. No
interaction with the people involved, including unsolicited interaction with
those enforcing the Code of Conduct, for a specified period of time. This
includes avoiding interactions in community spaces as well as external channels
like social media. Violating these terms may lead to a temporary or
permanent ban.

### 3. Temporary Ban

**Community Impact**: A serious violation of community standards, including
sustained inappropriate behavior.

**Consequence**: A temporary ban from any sort of interaction or public
communication with the community for a specified period of time. No public or
private interaction with the people involved, including unsolicited interaction
with those enforcing the Code of Conduct, is allowed during this period.
Violating these terms may lead to a permanent ban.

### 4. Permanent Ban

**Community Impact**: Demonstrating a pattern of violation of community
standards, including sustained inappropriate behavior,  harassment of an
individual, or aggression toward or disparagement of classes of individuals.

**Consequence**: A permanent ban from any sort of public interaction within
the community.

## Attribution

This Code of Conduct is adapted from the [Contributor Covenant][homepage],
version 2.0, available at
<https://www.contributor-covenant.org/version/2/0/code_of_conduct.html>.

Community Impact Guidelines were inspired by [Mozilla's code of conduct
enforcement ladder](https://github.com/mozilla/diversity).

For answers to common questions about this code of conduct, see the FAQ at
<https://www.contributor-covenant.org/faq>. Translations are available at
<https://www.contributor-covenant.org/translations>.

[code-of-conduct-contact]: mailto:code-of-conduct@swindle.net?subject=%5Bubuntu-gradle-java-node%5D%20Code%20of%20Conduct%20Concern(s)&body=Please%20describe%20any%20instances%20of%20abusive%2C%20harassing%2C%20or%20otherwise%20unacceptable%20behavior%20(as%20described%20at%20https%3A%2F%2Fgitlab.com%2Fdata-leakage-protection%2Fci-cd%2Fubuntu-gradle-java-node%2F-%2Fblob%2Fmaster%2FCODE_OF_CONDUCT.md).%20All%20complaints%20will%20be%20reviewed%20and%20investigated%20promptly%20and%20fairly.%0D%0A%0D%0AYOUR%20PRIVACY%20AND%20SECURITY%3A%0D%0AWe%20respect%20your%20privacy%20and%20security%2C%20and%20promise%20to%20keep%20your%20identity%20anonymous%2C%20unless%20you%20agree%20otherwise%20in%20writing.%0D%0A%0D%0A%0D%0A%0D%0A%3C!--%20%F0%9F%9A%AB%20Please%20do%20not%20remove%20this%20line%20or%20anything%20under%20it.%20%F0%9F%9A%AB%20--%3E%0D%0A%0D%0A-%20Referring%20repository%3A%20ubuntu-gradle-java-node%20%3Chttps%3A%2F%2Fgitlab.com%2Fdata-leakage-protection%2Fci-cd%2Fubuntu-gradle-java-node%3E%0D%0A

[homepage]: https://www.contributor-covenant.org
